defmodule SVG do

  @moduledoc """
  Renders a figure as a SVG string.

  To render the figure; its boundingbox is computed so that the whole
  figure is mapped onto the SVG. The size of the SVG image depends on
  the size of the boundingbox and og the grid size. Grid size
  represent the "unity", that is the rendered size (see it as the
  number of pixels even if it has little sense as SVG is vector
  format) of segments `[(0, 0) - (1, 0)]` on the `X` axis and `[(0, 0)
  - (0, 1)]` on the `Y` axis. For example, if point *P* has the
  coordinates *P2(0, 1)*, if the
  grid on *X* has a size of 10, its coordinates in the SVG space
  will be *(0, 10)*.

  The basis vectors in Figures are as in the catesian frame: 
  horizontal toward the right for growing *x*s and 
  and vertical toward the top for *y*s. In SVG, the direction of the *Y*
  is opposite so all the coordinates are recalculated during rendering.

  As all elements in the library, SVG accepts some properties in the
  `new()` function. Those properties are not regular SVG properties:

  * `bg: <color>`: defines the color of the background which otherwise
    is transparent.
  * `border: <number>`: adds a border of size `number`.
  * `grid: :front | :back` : if present adds a grid in light gray either 
    on top of the figure or on bottom.
  * `grid_step: n` : if present draws only every n grid lines.

  Styling of the elements in the figure can be either done direclty
  when the each element is defined and added to the figureor, once for
  all, with a CSS stylesheet that is to be added to the SVG object
  before rendering.
  """
  
  alias Geometry.Figure
  
  defstruct figure: nil,
    hgrid: 0, vgrid: 0,
    props: [],
    xprops: [],
    styles: []


  @doc """
  Returns a new SVG associated with the figure `f`. Parameters `hg` (aka 
  *horizontal grid*) and `vg` (aka 
  *vertical grid*) define the size of the unit in SVG space. `new()` also 
  accepts optional parameters:

  * `bg: <color>`: defines the `color` of the background which otherwise
    is transparent.
  * `border: <integer>`: defines the size of the border in pixels. No
    border by default
  * `grid: :front | :back` : if present adds a grid in light gray either 
    on top of the figure or on bottom.
  * `grid_step: n` : if present draws only every n grid lines.
  """
  
  def new(%Figure{}=f, hg, vg, opts \\ []) do
    %SVG{figure: f, hgrid: hg, vgrid: vg, xprops: opts}
  end

  
  @doc """
  Adds a stylesheet to the image. `css` has to be a valid CSS string
  """
  def add_style(%SVG{}=svg, css), do: %SVG{svg | styles: [css | svg.styles]}

  
  @docp """
  Converts the coordinates `(x,y)` to fit the svg image 
  and its grid size
  """
  defp convert(x, y, height, min_x, min_y, hg, vg, bsz) do
    {
      Kernel.round(((x - min_x) * hg)+bsz),
      Kernel.round(((height - (y - min_y)) * vg)+bsz)
    }
  end

  @docp """
  Transforms the tuple `{key: :value}` into the string `key="value"`.
  """
  defp tuple2svg({key, value}), do: ~s|#{key}="#{value}"|

  
  @docp """
  Generates a list of SVG strings for drawing endpoints
  """
  defp mk_endpoints(eps, height, min_x, min_y, hg, vg, bsz) do
    Enum.map(eps, &prepare(&1, height, min_x, min_y, hg, vg, bsz))
  end
  
  @docp """
  Generates a list of SVG strings for drawing endpoints. 
  Styles first every point with `style(point, index)` where `index`
  is the rank of this point in the list
  """
  defp mk_endpoints(eps, style, height, min_x, min_y, hg, vg, bsz) do
    {_n, eps} = Enum.reduce(eps, {0, []}, fn(ep, {n, acc}) ->
      p = style.(ep, n)
      |> prepare(height, min_x, min_y, hg, vg, bsz)
      {n+1, [p | acc]}
    end)
    Enum.reverse(eps)
  end

  @docp """ 
  Transforms each element from the `Figure` into a list of SVG
  strings. `prepare()` also converts the figure's coordinates into
  SVG's coordinates.

  Parameters:
  
  * `first`: the first parameter can be one of
    * a `Point`. Points don't exist in SVG
      and will be rendered as circles. A special property `is-point` 
      is set to `1` so that they can be treated properly by CSS. 
    * a `Segment`. Segments don't exist in SVG. a segment will be rendered 
      as a line
    * a `Circle`. Circles are rendered as 
      ellipses so that they are represented properly when `xgrid` and `ygrid`
      are not equal. A special property `is-circle` is set to `1` so that
      they can be treated properly by CSS. 
    * an `Ellipse`.
    * an `Polyline`. 
    * an `Arc`. 
  * `height`: the height of the figure
  * `min_x` and `min_y`: the inferior coordinates of the bounding box
  * `hg` and `vg`: the size of the unit in SVG space
  * `bsz`: the size of the border in pixels
  """
  
  defp prepare(nil, _height, _min_x, _min_y, _hg, _vg, _bsz), do: ""  
  
  @point_defaults ["is-point": 1, r: 1, stroke: :black, fill: :black, "stroke-width": 0]
  defp prepare(%Geometry.Point{}=p, height, min_x, min_y, hg, vg, bsz) do
    {x, y} = convert(p.x, p.y, height, min_x, min_y, hg, vg, bsz)
    props = [cx: x, cy: y]
    |> Keyword.merge(@point_defaults)
    |> Keyword.merge(p.props)
    |> Enum.reduce("", fn(kv, acc) -> "#{acc} #{tuple2svg(kv)}" end)
    "<circle #{props} />"
  end
  
  @segment_defautls [stroke: :black, "stroke-width": 1]
  defp prepare(%Geometry.Segment{}=s, height, min_x, min_y, hg, vg, bsz) do
    {x1, y1} = convert(s.p1.x, s.p1.y, height, min_x, min_y, hg, vg, bsz)
    {x2, y2} = convert(s.p2.x, s.p2.y, height, min_x, min_y, hg, vg, bsz)
    props = [x1: x1, y1: y1, x2: x2, y2: y2]
    |> Keyword.merge(@segment_defautls)
    |> Keyword.merge(s.props)
    |> Enum.reduce("", fn(kv, acc) -> "#{acc} #{tuple2svg(kv)}" end)
    seg = "<line #{props} />"
    
    # if we need to display endpoints:
    ep = case s.xprops[:endpoint] do
           nil ->
             []
           %Geometry.Point{}=p ->
             eps = [
               Geometry.Point.set_xy(p, s.p1.x, s.p1.y),
               Geometry.Point.set_xy(p, s.p2.x, s.p2.y)
             ]
             mk_endpoints(eps, height, min_x, min_y, hg, vg, bsz)
           style ->
             eps = [s.p1, s.p2]
             mk_endpoints(eps, style, height, min_x, min_y, hg, vg, bsz)
         end
    "#{seg}\n#{Enum.join(ep, "\n")}"
  end

  @ellipse_defautls [fill: :none, stroke: :black, "stroke-width": 1]
  defp prepare(%Geometry.Ellipse{}=e, height, min_x, min_y, hg, vg, bsz) do
    {cx, cy} = convert(e.cx, e.cy, height, min_x, min_y, hg, vg, bsz)
    props = [cx: cx, cy: cy, rx: e.rx*hg, ry: e.ry*vg]
    |> Keyword.merge(@ellipse_defautls)
    |> Keyword.merge(e.props)
    |> Enum.reduce("", fn(kv, acc) -> "#{acc} #{tuple2svg(kv)}" end)
    "<ellipse #{props} />"
  end

  @circle_defautls ["is-circle": 1, fill: :none, stroke: :black, "stroke-width": 1]
  defp prepare(%Geometry.Circle{}=c, height, min_x, min_y, hg, vg, bsz) do
    {cx, cy} = convert(c.cx, c.cy, height, min_x, min_y, hg, vg, bsz)
    props = [cx: cx, cy: cy, rx: c.r*hg, ry: c.r*vg]
    |> Keyword.merge(@circle_defautls)
    |> Keyword.merge(c.props)
    |> Enum.reduce("", fn(kv, acc) -> "#{acc} #{tuple2svg(kv)}" end)
    "<ellipse #{props} />"
  end

  @polyline_defautls [fill: :none, stroke: :black, "stroke-width": 1]
  defp prepare(%Geometry.Polyline{}=pl, height, min_x, min_y, hg, vg, bsz) do
    coords = Enum.reduce(pl.points, "", fn(p, acc) ->
      {x, y} = convert(p.x, p.y, height, min_x, min_y, hg, vg, bsz)
      "#{acc} #{x},#{y}"
    end)
    props = [points: coords]
    |> Keyword.merge(@polyline_defautls)
    |> Keyword.merge(pl.props)
    |> Enum.reduce("", fn(kv, acc) -> "#{acc} #{tuple2svg(kv)}" end)
    pline = "<polyline #{props} />"
    
    # if we need to display endpoints:
    ep = case pl.xprops[:endpoint] do
           nil ->
             []
           %Geometry.Point{}=p ->
             mk_endpoints(Enum.map(pl.points, &Geometry.Point.set_xy(p, &1.x, &1.y)), height, min_x, min_y, hg, vg, bsz)
           style ->
             mk_endpoints(pl.points, style, height, min_x, min_y, hg, vg, bsz)
         end
    "#{pline}\n#{Enum.join(ep, "\n")}"
  end  

  # This one is tricky as an arc is represented with its endpoint
  # coordinates and not as a part of an ellipse
  # see the doc for center2endpoint()
  @arc_defautls [fill: :none, stroke: :black, "stroke-width": 1] 
  defp prepare(%Geometry.Arc{}=a, height, min_x, min_y, hg, vg, bsz) do

    # First convert the parameters to the svg space...
    {cx, cy} =  convert(a.cx, a.cy, height, min_x, min_y, hg, vg, bsz)
    {rx, ry} = {a.rx*hg, a.ry*vg}
    # ... then calculate the parameters for endpoint parametrization
    {x1, y1, fa, fs, x2, y2} = center2endpoint(cx, cy, rx, ry,
      a.phi, a.theta1, a.theta2)

    # Generate the path
    props = [d: "M #{x1} #{y1} A #{a.rx*hg} #{a.ry*vg} #{a.phi} #{fa} #{fs} #{x2} #{y2}"]
    |> Keyword.merge(@arc_defautls)
    |> Keyword.merge(a.props)
    |> Enum.reduce("", fn(kv, acc) -> "#{acc} #{tuple2svg(kv)}" end)
    arc = "<path #{props} />"

    # if we need to display endpoints:
    ep = case a.xprops[:endpoint] do
           nil ->
             []
           %Geometry.Point{}=p ->
             eps = [
               Geometry.Point.set_xy(p, 0, 0),
               Geometry.Point.set_xy(p, 0, 0)
             ]
             
             # This is really a quick and dirty hack!
             # This is due to the fact that the coordinates of the endpoints are 
             # calculated *after* the arc had been ajusted to the SVG frame
             # Then adding point based on these coordinates leads to false coordinates
             # for the endpoints
             [p1, p2] = mk_endpoints(eps, height, min_x, min_y, hg, vg, bsz)
             p1 = p1
             |> String.replace(~r/cx="\d*"/, ~s|cx="#{x1}"|)
             |> String.replace(~r/cy="\d*"/, ~s|cy="#{y1}"|)
             p2 = p2
             |> String.replace(~r/cx="\d*"/, ~s|cx="#{x2}"|)
             |> String.replace(~r/cy="\d*"/, ~s|cy="#{y2}"|)
             [p1, p2]
           style ->
             eps = [Geometry.Point.new(), Geometry.Point.new()]
             [p1, p2] = mk_endpoints(eps, style, height, min_x, min_y, hg, vg, bsz)
             p1 = p1
             |> String.replace(~r/cx="\d*"/, ~s|cx="#{x1}"|)
             |> String.replace(~r/cy="\d*"/, ~s|cy="#{y1}"|)
             p2 = p2
             |> String.replace(~r/cx="\d*"/, ~s|cx="#{x2}"|)
             |> String.replace(~r/cy="\d*"/, ~s|cy="#{y2}"|)
             [p1, p2]
         end
    "#{arc}\n#{Enum.join(ep, "\n")}"
  end

  @docp """
  For conversion from center to endpoint parameterization, see
  * https://www.w3.org/TR/SVG/implnota.html#ArcImplementationNotes
  * https://observablehq.com/@toja/ellipse-and-elliptical-arc-conversion
  """
  defp center2endpoint(cx, cy, rx, ry, phi, theta1, theta2) do
    phi = Math.deg2rad(phi)
    t1 = Math.deg2rad(theta1)
    t2 = Math.deg2rad(theta2)
    
    cos_phi = :math.cos(phi)
    sin_phi = :math.sin(phi)
    cos_t1 = :math.cos(t1)
    sin_t1 = :math.sin(t1)
    cos_t2 = :math.cos(t2)
    sin_t2 = :math.sin(t2)

    # Conversion from center to endpoint parameterization
    x1 = cos_phi*rx*cos_t1 - sin_phi*ry*sin_t1 + cx
    y1 = sin_phi*rx*cos_t1 + cos_phi*ry*sin_t1 + cy
    x2 = cos_phi*rx*cos_t2 - sin_phi*ry*sin_t2 + cx
    y2 = sin_phi*rx*cos_t2 + cos_phi*ry*sin_t2 + cy
    
    fa = if Math.abs(theta1-theta2) > 180, do: 1, else: 0
    fs = if theta1-theta2 <= 0, do: 1, else: 0

    {x1, y1, fa, fs, x2, y2}
  end

  @docp """
  Generates a grid for the SVG.

  Parameters are:
  * `w` the width of the image
  * `h` the height of the image
  * `bsz` the size of the border of the image
  * `dx` the the pace of the grid in the *X* direction
  * `dy` the the pace of the grid in the *Y* direction
  * `step` every how many steps to draw a line of the grid
  """
  defp mk_grid(w, h, bsz, dx, dy, step) do
    hlines = 0..Kernel.round(w)
    |> Enum.map(&(&1*dx))
    |> Enum.take_every(step)
    |> Enum.reduce("", fn(x, acc) ->
      line = ~s|x1="#{x+bsz}" y1="#{bsz}" x2="#{x+bsz}" y2="#{bsz+h*dy}"|
      opts = ~s|stroke="lightgrey" |
      "#{acc}<line #{line} #{opts} />\n"
    end)

    vlines = 0..Kernel.round(h)
    |> Enum.map(&(&1*dy))
    |> Enum.take_every(step)
    |> Enum.reduce("", fn(y, acc) ->
      line = ~s|x1="#{bsz}" y1="#{y+bsz}" x2="#{bsz+w*dx}" y2="#{y+bsz}"|
      opts = ~s|stroke="lightgrey" |
      "#{acc}<line #{line} #{opts} />\n"
    end)

    "\n<!-- The grid -->\n#{hlines}#{vlines}<!-- End of grid -->\n"
  end
  
  @doc """
  Generate the SVG string out of the SVG image
  """
  def render(%SVG{}=svg) do
    {bbx1, bby1, bbx2, bby2} = Figure.bounding_box(svg.figure)
    width = (bbx2-bbx1)
    height = (bby2-bby1)

    # process eXtented properties
    bg = if svg.xprops[:bg] != nil do
      ~s|<rect width="100%" height="100%" fill="#{svg.xprops[:bg]}" />|
    else
      ""
    end

    bsz = Keyword.get(svg.xprops,:border, 0)

    grid_step = Keyword.get(svg.xprops,:grid_step, 1)
    grid = if svg.xprops[:grid] == nil do
      nil
    else
      mk_grid(width, height, bsz, svg.hgrid, svg.vgrid, grid_step)
    end

    # process the figures from the figure
    elems = Enum.map(svg.figure.elements,
      &prepare(&1, height, bbx1, bby1, svg.hgrid, svg.vgrid, bsz))
      |> Enum.reverse

    # process the styles
    style_cnt = Enum.reduce(svg.styles, "", fn(s,a) -> "#{a}\n#{s}" end)
    |> String.trim()
    style = "<style>\n#{style_cnt}\n</style>"

    fig_content = elems
    |> Enum.reduce("", fn(s, acc) -> "#{acc}\n#{s}" end)

    content = if(svg.xprops[:grid] == :front) do
      "#{bg}\n#{fig_content}\n#{grid}"
    else
      "#{bg}\n#{grid}\n#{fig_content}"
    end
      

    """
    <svg
      version="2.0"
      width="#{Kernel.round(width*svg.hgrid+2*bsz)}"
      height="#{Kernel.round(height*svg.vgrid+2*bsz)}"
      xmlns="http://www.w3.org/2000/svg">

      #{if style_cnt == "", do: "", else: style}

      #{content}
    </svg>
    """
    
  end

  @doc """
  Writes the SVG string to a file.

  Simply a wrapper around `File.write` to have the SVG string 
  as the first element to make pipes (`|>`) happy.
  """
  def write(svg_string, path), do: File.write(path, svg_string)

end
