defmodule Math.Prime do

  @moduledoc """
  This module defines functions related to prime numbers.
  """

  @doc """
  Returns `true` if `n` is prime and `false` otherwise.
  
  See http://rosettacode.org/wiki/Primality_by_trial_division#Elixir
  """
  def is_prime(2), do: true
  def is_prime(n) when n<2 or rem(n,2)==0, do: false
  def is_prime(n), do: is_prime(n,3)
  defp is_prime(n,k) when n<k*k, do: true
  defp is_prime(n,k) when rem(n,k)==0, do: false
  defp is_prime(n,k), do: is_prime(n,k+2)

  
  @doc """
  Computes the list of the first n primes. See

  * http://rosettacode.org/wiki/Prime_conspiracy#Elixir and
  * https://en.wikipedia.org/wiki/Rosser's_theorem
  """
  def primes(n) do
    lnn = :math.log(n)
    # Pierre Dusart's lower bound rather than Rosser's
    max = n * (lnn + :math.log(lnn) -1) |> trunc  
    Enum.to_list(2..max)
    |> primes(:math.sqrt(max), [])
    |> Enum.take(n)
  end
  defp primes([h|t], limit, result) when h>limit, do: Enum.reverse(result, [h|t])
  defp primes([h|t], limit, result) do
    primes((for x <- t, rem(x,h)>0, do: x), limit, [h|result])
  end

  
  @doc """
  Computes the list of the prime factors of `n`.
  See: http://rosettacode.org/wiki/Prime_decomposition
  """
  def factors(n), do: factors(n, 2, [])
  defp factors(n, k, acc) when n < k*k, do: Enum.reverse(acc, [n])
  defp factors(n, k, acc) when rem(n, k) == 0, do: factors(div(n, k), k, [k | acc])
  defp factors(n, k, acc), do: factors(n, k+1, acc)

  
  @doc """
  Computes the GCD `n1` and `n2`. 
  See: http://rosettacode.org/wiki/Greatest_common_divisor#Elixir
  """
  def gcd(a,0), do: abs(a)
  def gcd(a,b), do: gcd(b, rem(a,b))
end
