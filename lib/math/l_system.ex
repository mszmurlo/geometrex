defmodule Math.LSystem do

  @moduledoc """
  Implementation of L-system.

  An `LSystem` is defined by an axiom string and a set of rules (a list
  of tuples with two strings). A symbol that is a left part of a rule is
  considered as a *variable*; a symbol that is not a left part of any rule
  is a *terminal*. During the substitution process, variables are substituted
  with the corresponding right part of the matching rule while terminals
  are simply copied. The substitution process substitutes all variables
  in the input string (that is the result of the previous execution) only once.

  There is almost no integrity check on the elements of the `LSystem` 
  for the moment. For exemple, if two rules with the same left part are defined
  only the first one will apply
  """

  defstruct rules: [], result: []

  @doc """
  Defines a new `LSystem`.

  * `axiom` is a non empty string that may contain any character.
  * `rules` is a list of tuples of the form `[{left_part, right_part}, ...]` where
  `left_part` is a single character and `right_part` the string that will replace
  the left part in the axiom.
  
  Returns a `%Math.LSystem{}` structure. For example:
  ```
  ex(597)> ls=Math.LSystem.new("F", [{"F", "AFB"}])
  %Math.LSystem{
    result: ["F"], 
    rules: [{"F", "AFB"}]
  }
  ```
  During the creation of the `LSystem`, the `axiom` becomes the first result of
  the substitution
  """
  def new("", _rules), do: ArgumentError
  def new(_axiom, []), do: ArgumentError
  def new(axiom, rules) do
    %Math.LSystem{rules: rules, result: [axiom]}
  end

  def last_result(%Math.LSystem{result: res}), do: hd(res)
  
  def all_results(%Math.LSystem{result: res}), do: res

  
  @doc """
  Runs once the substitution process on the `LSystem`. `ls` is the `LSystem` to
  process. 

  Returns a `LSystem` with a the result of the substitution stored in the retult list
  """
  def run(%Math.LSystem{}=ls) do
    %Math.LSystem{ls | result: [runp(ls.rules, hd(ls.result)) | ls.result]}
  end

  
  @doc """
  Runs `n` times the substitution process on the `LSystem` `ls`

  Returns a `LSystem` with a the result of the substitution stored in the retult list
  """
  def run(%Math.LSystem{}=ls, n) when n>0 do
    res = Enum.reduce(1..n, ls.result, fn(n, acc) ->
      [runp(ls.rules, hd(acc)) | acc]
    end)
    %Math.LSystem{ls | result: res}
  end

  # Returns the result of the run as a string
  def runp(rules, start) do
    Enum.reduce(start |> String.split("", trim: true), "", fn(c, acc) ->
      # IO.puts("c=#{c}")
      rule = Enum.find(rules, {nil, nil}, fn({lp, _rp}) -> c == lp end)
      # IO.puts("r=#{inspect rule}")
      case rule do
        {nil, nil} -> acc <> c
        {_lp, rp} -> acc <> rp
      end
    end)
  end

end
