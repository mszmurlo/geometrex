defmodule Math.Walk do

  @moduledoc """
  This module defines functions that pair **N** with **N^2**, that is that
  associate uniquely an interger **n** with a a couple of integers **(x, y)**.
  **(x, y)** will be treated as coordinates in the plane.
  """

  @doc """
  For a given positive  integer `n`, returns the pair `(x, y)` on the
  Ulam spriral. 

  Returns `ArgumentError` if n is not a strictly positive  integer

  See: https://en.wikipedia.org/wiki/Ulam_spiral.
  """
  
  def ulam_n2xy(1), do: {0, 0}
  def ulam_n2xy(n) when is_integer(n) and n>1 do
    # r is the "ring" on which n sits
    r = floor((:math.sqrt(n-1)+1) / 2)
    # (number of numbers on each side of ring r) - 1
    sr = 2*r
    # the first number on the ring r
    f = (2*r-1)*(2*r-1) + 1 
    cond do
      n <= f+1*sr-1 -> {r, r-(f+1*sr-1-n)}
      n <= f+2*sr-1 -> {(f+2*sr-1-n)-r, r}
      n <= f+3*sr-1 -> {-r, (f+3*sr-1-n)-r}
      n <= f+4*sr-1 -> {r-(f+4*sr-1-n), -r}
    end
  end
  def ulam_n2xy(_n), do: ArgumentError


  
  @doc """
  For a given positive  integer `n`, returns the pair `(x, y)`. 

  Returns `ArgumentError` if n is not a positive or null integer

  See: https://en.wikipedia.org/wiki/Pairing_function
  """
  def cantor_n2xy(n) when is_integer(n) and n>=0 do
    w = :math.floor((:math.sqrt(8*n+1)-1)/2)
    t = (w*w + w) / 2
    y = n-t
    x = w - y
    {trunc(x), trunc(y)}
  end
  def cantor_n2xy(_n), do: ArgumentError

end
