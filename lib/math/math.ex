defmodule Math do

  @moduledoc """  
  The `Math` module defines math functions.
  Unclassified functions are defined directly 
  in the `Math` module while other fuctions are grouped 
  by topic in sub-modules.
  """

  @doc """
  Converts degrees to radians
  """
  def deg2rad(deg), do: deg * (2 * :math.pi) / 360

  @doc """
  returns the absolute value of x
  """
  def abs(x), do: if x<0, do: -x, else: x

  @doc """
  returns the minimum and the maximum of the two values
  as a tuple `{min max}`.
  """
  def min_max(x,y), do: if x<=y, do: {x, y}, else: {y, x}

  @doc """
  Calculates the *repeated digital sum* of `n` that is the sum of `n`'s
  digits and then the sum of the sum, and so on until the result is one digit
  """
  def rds(n) when n<0, do: -rds(-n)
  def rds(n) when n<10, do: n
  def rds(n), do: Integer.digits(n) |> Enum.sum |> rds


end
