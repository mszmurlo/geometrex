defmodule Geometry.Circle do

  @moduledoc """
  Module for handling circles.

  A circle is defined by the coordinates  `cx` and `cy` of its center
  and by its radius `r`.

  Cirle's properties are

  * `fill:`: is the fill color of the circle
  * `stroke:`: the color of the stroke
  * `"stroke-width":`: the width of the stroke. (Notice the syntax
    `"stroke-width":` and not `stroke-width:` which is not a valid 
    atom definition in Elixir

  """
  
  @behaviour Geometry

  defstruct cx: 0, cy: 0, r: 0, props: []

  @doc """
  Returns the circle of radius `r`, which center is at 
  coordinates `(cx, cy)`.
  
  Optional parameters are:
  * all SVG presentation parameters allowed for a `ellipse` element
  """
  def new(cx, cy, r, opts \\ []) do
    %Geometry.Circle{cx: cx, cy: cy, r: r, props: opts}
  end
  
  @doc """
  Adds properties to the circle
  """
  def add_props(c, prop \\ []) do
    %{c | props: Keyword.merge(c.props, prop)}
  end

  
  @doc """
  Returns the bounding box of the circle.
  """
  def bounding_box(%Geometry.Circle{}=c) do
    {-c.r, -c.r, c.r, c.r}
  end
  
end
