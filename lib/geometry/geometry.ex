defmodule Geometry do

  @doc """
  Documentation for the module `Geometry`
  """

  
  @callback bounding_box(term) :: {number, number, number, number}

  @doc """
  Returns a tuple `{x1, y1, x2, y2}` which defines the bounding box of
  the geometry element
  """
  def bbox(element) do
    typeof(element).bounding_box(element)
  end
  
##  @callback normalise_frame(term, number, number, number, number, number) :: term

##  @doc """
##  Normalises the frame for SVG: Origin will be moved from
##  left-bottom to left-top
##  """
##  def normalise_frame(element, xm, ym, h, sw, sh) do
##    typeof(element).normalise_frame(element, xm, ym, h, sw, sh)
##  end
  
  defp typeof(%module{}) do
    module
  end
  
end
