defmodule Geometry.Segment do

  alias Geometry.Point
  alias Geometry.Util
  @behaviour Geometry

  @moduledoc """
  Module for handling segments.

  A segment is defined by two points. It will be rendered as a line so
  it has line's properties:

  * `stroke:`: the color of the stroke

  * `"stroke-width":`: the width of the stroke. (Notice the syntax
    `"stroke-width":` and not `stroke-width:` which is not a valid 
    atom definition in Elixir

  * `endpoint:`: is a *pseudo*-property. It holds a geometric a
    `Point` that will be rendered at the begining and at the end of
    the segment. For exemple:

    ```
    f = Figure.new
    |> Figure.add(Segment.new(0, 0, 4, 4, 
                    stroke: :red, "stroke-width": 2,
                    endpoint: Point.new(0, 0, r: 5, fill: :black)))
    ```

    `endpoint` can also be a function which takes two arguments: a
    `Point` that specifies the position where to render the endpoint
    and the index of this endpoint on the `Segment`. For exemple:

    ```
    ep = fn(%Point{}=p, n) ->
      if n == 0 do
        Point.new(p.x, p.y, r: 5, fill: :red)
      else
        Point.new(p.x, p.y, r: 3, fill: :green)
      end
    Segment.new(0, 0, 10, 0, endpoint: ep, stroke: :blue)
    ```

    While theoretically any other element than a `Point` could be used
    as an endpoint, the current implementation is limited to the usage of `Point`s.

  """
  
  defstruct p1: %Point{}, p2: %Point{}, props: [], xprops: []

  @doc """
  Defines a new segment with two `Point`s.
  
  Optional parameters are:
  * all SVG presentation parameters allowed for a `line` element
  * `endpoint:`: see the module documentation
  """
  def new(%Point{}=p1, %Point{}=p2, opts \\ []) do
    {props, xprops} = Util.extract_props(opts, [:endpoint])
    %Geometry.Segment{p1: p1, p2: p2, props: props, xprops: xprops}
  end

  @doc """
  Defines a new segment with coordinates of the endpoints.
  
  Optional parameters are:
  * all SVG presentation parameters allowed for a `line` element
  * `endpoint:`: see the module documentation
  """
  def new(x1, y1, x2, y2, opts \\ []) do
    {props, xprops} = Util.extract_props(opts, [:endpoint])
    %Geometry.Segment{
      p1: Point.new(x1, y1),
      p2: Point.new(x2, y2),
      props: props,
      xprops: xprops}
  end
  
  @doc """
  Returns the middle point of the segment `s`.
  """
  def middle(%Geometry.Segment{}=s) do
    %Point{x: (s.p1.x + s.p2.x)/2, y: (s.p1.y + s.p2.y)/2}
  end

  @doc """
  Returns the length of the segment
  """
  def length(%Geometry.Segment{p1: p1, p2: p2}=_s) do
    dx = p2.x - p1.x
    dy = p2.y - p1.y
    :math.sqrt(dx*dx + dy*dy)
  end
  
  @doc """
  Adds properties to the segment
  """
  def add_props(seg, p \\ []) do
    %{seg | props: Keyword.merge(seg.props, p)}
  end
  
  @doc """
  Returns the bounding box of the segment, which is
  `{x1, y1, x2, y2}` where `x1` and `y1` are the minimas 
  of the coordinates of both ends of the segments 
  and `x2` and `y2` are the maximas
  """
  def bounding_box(%Geometry.Segment{}=s) do
    {x1, x2} = Enum.min_max([s.p1.x, s.p2.x])
    {y1, y2} = Enum.min_max([s.p1.y, s.p2.y])
    {x1, y1, x2, y2}
  end 

end
