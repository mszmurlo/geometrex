defmodule Geometry.Figure do

  @moduledoc """

  Functions to manipulate figures. A figure is a list of graphical
  objects like points, segments, circles, etc..

  The basis vectors are as in the catesian frame: horizontal to the right for `x`
  and vertical to the top for `y`.

  Each graphical element that is added to a figure can be slyled with
  any allowed of SVG property for that element. For exemple to define a circle
  located at (10, 5) with radius 5 and with a blue perimeter, write

  ```
  c = Circle.new(10, 5, r: 5, stroke: :blue)
  ```

  Colors can be either svg colors written as elixir atoms (eg: `:blue`
  for `"blue"`) or as RGB (eg: `"RGB(0, 255, 128)"`).

  Be aware that some SVG properties cannot be represented directly as
  Elixir atoms. This is the case for the proposerties with compound names
  as `"stroke-width"` which needs to be written `"stroke-width":`. Another 
  example is the color definition as RGB values just as above.

  For some elements there are *pseudo-propserties* that help their rendering.

  """
  
  defstruct elements: [],
    props: %{}

  @doc """
  Creates a new figure.
  """  
  def new(props \\ []) do
    %Geometry.Figure{props: props}
  end
  
  @doc """
  Adds one or more graphical element to the figure
  """
  def add(fig, []), do: fig

  def add(fig, [e | rest]) do
    f = add(fig, e)
    add(f, rest)
  end
    
  def add(fig, elem) do
    %{fig | elements: [elem | fig.elements]}
  end
  
  @doc """
  Adds properties to the figure
  """
  def add_props(fig, p \\ []) do
    %{fig | props: Keyword.merge(fig.props, p)}
  end

  @doc """
  Returns the bounding box of the figure, which is the tuple
  `{x1, y1, x2, y2}` where `x1` and `y1` are the minimas of 
  the coordinates of all ends of the elements of the figure 
  and `x2`, `y2` are the mamimas.
  """
  def bounding_box(%Geometry.Figure{}=fig) do
    minmax = Geometry.bbox(hd(fig.elements))
    Enum.reduce(tl(
      fig.elements),
      minmax,
      fn(fe, {x1, y1, x2, y2}) ->
        {xx1, yy1, xx2, yy2} = Geometry.bbox(fe)
        {xmin, xmax} = Enum.min_max([x1, x2, xx1, xx2])
        {ymin, ymax} = Enum.min_max([y1, y2, yy1, yy2])
        {xmin, ymin, xmax, ymax}
      end
    )
  end
  
end
