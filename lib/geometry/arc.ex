defmodule Geometry.Arc do

  @moduledoc """
  Module for handling elliptic arcs.

  An arc is defined by the following parameters
  * `cx` and `cy` the center of the ellipse
  * `rx` and `ry` the radii of the ellipse
  * `phi` the angle  between the X-axis of the frame to the 
    X-axis of the ellipse
  *  `theta1` the start angle of the arc
  *  `theta2` the end angle of the arc

  Notice that unlike in SVG, angles are in **degrees** and 
  **counter clockwise**.

  Arc's properties are

  * `fill:`: is the fill color of the circle
  * `stroke:`: the color of the stroke
  * `"stroke-width":`: the width of the stroke. (Notice the syntax
    `"stroke-width":` and not `stroke-width:` which is not a valid 
    atom definition in Elixir

  * `endpoint:`: is a *pseudo*-property. It holds a geometric a
    `Point` that will be rendered at the begining and at the end of
    the arc. For exemple:

    ```
    f = Figure.new
    |> Figure.add(Arc.new(0, 0, 4, 4, 0, 0, 90, 
                          stroke: :red, "stroke-width": 2,
                          endpoint: Point.new(0, 0, r: 5, fill: :black)))
    ```

    `endpoint` can also be a function which takes two arguments: a
    `Point` that specifies the position to render the endpoint and the
    index of this endpoint on the `Arc`. For exemple:

    ```
    ep = fn(%Point{}=p, n) ->
      if n == 0 do
        Point.new(p.x, p.y, r: 5, fill: :red)
      else
        Point.new(p.x, p.y, r: 3, fill: :green)
      end

    f = Figure.new()
    |> Figure.add(Arc.new(10, 10, rx, ry, 20, phi, phi+10,
            stroke: :red, "stroke-width": 3,
            endpoint: ep))
    ```

    While theoretically any other element than a `Point` could be used
    as an endpoint, the current implementation is limited to the usage of `Point`s.

  If you are interested in the implementation, see
  https://www.w3.org/TR/SVG/implnote.html#ArcImplementationNotes as
  it's a bit tricky.
  """
  
  alias Geometry.Util
  @behaviour Geometry

  defstruct cx: 0, cy: 0,
    rx: 0, ry: 0,
    phi: 0,
    theta1: 0, theta2: 360,
    props: [], xprops: []

  @doc """
  Returns a new arc.
  
  Optional parameters are:
  * all SVG presentation parameters allowed for a `path` element
  * `endpoint:` see module doc
  """
  def new(cx, cy, rx, ry, phi, theta1, theta2, opts \\ []) do
    {props, xprops} = Util.extract_props(opts, [:endpoint])
    %Geometry.Arc{
      cx: cx, cy: cy,
      rx: rx, ry: ry,
      phi: -phi,
      theta1: -theta1, theta2: -theta2,
      props: props, xprops: xprops}
  end
  
  @doc """
  Adds properties to the arc
  """
  def add_props(%Geometry.Arc{}=a, prop \\ []) do
    %{a | props: Keyword.merge(a.props, prop)}
  end

  
  @doc """
  Returns the bounding box of the arc.

  Actually, the returned bounding box will be fine for a circle but will 
  be bigger than an ellipse as it is calculated for the circle
  that includes the ellipse regardless of its angle to the X-axis.
  """
  def bounding_box(%Geometry.Arc{}=a) do
    {_, mr} = Math.min_max(a.rx, a.ry)
    {a.cx-mr, a.cy-mr, a.cx+mr, a.cy+mr}
  end
  
end
