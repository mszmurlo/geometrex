defmodule Geometry.Point do

  @moduledoc """
  Module for handling geometrical points.

  A geometrical point defined by its `x` and `y` coordinates. Points
  can be added *properties*. As points are rendered as circles, they
  share cirle's properties:

  * `r:`: points are rendered as circles. `r` represents the radius
  * `fill:`: is the fill color of the circle
  * `stroke:`: the color of the stroke
  * `"stroke-width":`: the width of the stroke. (Notice the syntax
    `"stroke-width":` and not `stroke-width:` which is not a valid 
    atom definition in Elixir

  """
  
  @behaviour Geometry

  defstruct x: 0, y: 0, props: [], xprops: []

  @doc """
  Returns the origin, ie: `O(0, 0)`.
  
  Optional parameters are:
  * all SVG presentation parameters allowed for a `circle` element
  """
  def new(opts \\ []), do: %Geometry.Point{props: opts}

  @doc """
  Returns the point `(x, y)`.
  
  Optional parameters are:
  * all SVG presentation parameters allowed for a `circle` element
  """
  def new(x, y, opts \\ []) do
    %Geometry.Point{x: x, y: y, props: opts}
  end
  
  
  @doc """
  changes the coordinates of a point
  """
  def set_xy(%Geometry.Point{}=p, x, y) do
    %Geometry.Point{x: x, y: y, props: p.props, xprops: p.xprops}
  end

  
  @doc """
  Adds properties to the point
  """
  def add_props(point, p \\ []) do
    %{point | props: Keyword.merge(point.props, p)}
  end

  
  @doc """
  Returns the bounding box of the figure, which for the 
  point obviously is `{p.x, p.y, p.x, p.y}`.
  """
  def bounding_box(%Geometry.Point{}=p) do
    {p.x, p.y, p.x, p.y}
  end

  
end
