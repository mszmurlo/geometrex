defmodule Geometry.Ellipse do

  @moduledoc """
  Module for handling Ellipses.

  An ellipse is defined by the coordinates  `cx` and `cy` of its center
  and by radiuses on `x` and `y` axes,  `rx` and `ry`.

  Ellipse's properties are

  * `fill:`: is the fill color of the circle
  * `stroke:`: the color of the stroke
  * `"stroke-width":`: the width of the stroke. (Notice the syntax
    `"stroke-width":` and not `stroke-width:` which is not a valid 
    atom definition in Elixir.

  """
  
  @behaviour Geometry

  defstruct cx: 0, cy: 0, rx: 0, ry: 0, props: []

  @doc """
  Returns the ellipse of radius `rx` and `ry`, which center is at 
  coordinates `(cx, cy)`.
  
  Optional parameters are:
  * all SVG presentation parameters allowed for a `ellipse` element
  """
  def new(cx, cy, rx, ry, opts \\ []) do
    %Geometry.Ellipse{cx: cx, cy: cy, rx: rx, ry: ry, props: opts}
  end
  
  @doc """
  Adds properties to the ellipse
  """
  def add_props(e, prop \\ []) do
    %{e | props: Keyword.merge(e.props, prop)}
  end

  
  @doc """
  Returns the bounding box of the ellipse.
  """
  def bounding_box(%Geometry.Ellipse{}=e) do
    {-e.rx, -e.ry, e.rx, e.ry}
  end
  
end
