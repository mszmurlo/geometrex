defmodule Geometry.Util do

  @moduledoc """
  Utility funtions for geometries.
  """

  @doc """
  From the optional parameter list in `new(...)` functions, creates
  the lists `props` and `xprops` of proposerties by removing the 
  x-properties listed in `xp_list`.

  Example:
  ```
  iex> extract_props([a: "a", b: "b", c: "c"], [:b]) 
  {[a: "a", c: "c"], [b: "b"]}
  ```
  """
  
  def extract_props(opts, xp_list), do: extract_props_p(opts, [], xp_list)

  defp extract_props_p(props, _xprops, []), do: {props, []}

  defp extract_props_p(props, xprops, [xp]) do
    xp1 = Keyword.get(props, xp)
    xprops = if xp1 == nil, do: xprops, else: Keyword.put(xprops, xp, xp1)
    props = Keyword.delete(props, xp)
    {props, xprops}
  end

  defp extract_props_p(props, xprops, [xp | rest]) do
    xp1 = Keyword.get(props, xp)
    xprops = if xp1 == nil, do: xprops, else: Keyword.put(xprops, xp, xp1)
    props = Keyword.delete(props, xp)
    extract_props_p(props, xprops, rest)
  end
    
end

  
