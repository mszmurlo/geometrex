defmodule Geometry.Polyline do

  @moduledoc """
  Module for handling polylines.

  A polyline is defined by a list of points. At least two points are
  needed to define a polyline.

  Polyline's properties are

  * `stroke:`: the color of the stroke
  * `"stroke-width":`: the width of the stroke. (Notice the syntax
    `"stroke-width":` and not `stroke-width:` which is not a valid 
    atom definition in Elixir
  * `endpoint:`: is a *pseudo*-property. It holds a geometric a
    `Point` that will be rendered at the begining and at the end of
    the `Polyline`. For exemple:

    ```
    f = Figure.new
    |> Figure.add(Polyline.new(
        [0, 0, 1, 0, 1, 1, -1, 1, -1, -1, 2, -1, 2, 2, -2, 2, -2, -2, 3, -2],
        stroke: :blue,
        "stroke-width": 2,
        endpoint: Point.new(fill: :orange, r: 5))
    ```

    `endpoint` can also be a function which takes two arguments: a
    `Point` that specifies the position to render the endpoint and the
    index of this endpoint in the `Polyline`. For exemple:

    ```
    colors = %{0 => :blue, 1 => :green, 3 => :red}
    f = Figure.new
    |> Figure.add(Polyline.new(
        [0, 0, 1, 0, 1, 1, -1, 1, -1, -1, 2, -1, 2, 2, -2, 2, -2, -2, 3, -2],
        stroke: :blue, "stroke-width": 2,
        endpoint: fn(p, n) -> 
          Point.new(p.x, p.y, r: 3*(n+1), fill: colors[rem(n, 3)]) end)
    ```

    While theoretically any other element than a `Point` could be used
    as an endpoint, the current implementation is limited to the usage of `Point`s.
  """
  
  @behaviour Geometry

  alias Geometry.Point
  alias Geometry.Util

  defstruct points: [], props: [], xprops: []

  @doc """
  Returns a new polyline. `pts` is a list of coordinates. 

  Optional parameters are:
  * all SVG presentation parameters allowed for a `polyline` element
  * `endpoint:` see module doc.
  """
  def new(pts, opts \\ []) when is_list(pts) do
    {props, xprops} = Util.extract_props(opts, [:endpoint])
    case pts do
      [] -> raise ArgumentError, message: "empty list of coordinates"
      [_] -> raise ArgumentError, message: "list with one coordinate only"
      _ -> %Geometry.Polyline{points: newp(pts, [], 0), props: props, xprops: xprops}
    end
  end

  defp newp([x, y], acc, n) do
    p = Point.new(x, y, nth: n)
    [p | acc] |> Enum.reverse
  end
  defp newp([x, y | rest], acc, n) do
    p = Point.new(x, y, nth: n)
    newp(rest,  [p | acc], n+1)
  end
  defp newp(_, _, _), do: raise ArgumentError, message: "bad number of coordinates"
 
  def add_points(%Geometry.Polyline{}=pl, pts) do
    points = newp(pts, [], Enum.count(pl.points))
    %{pl | points: pl.points ++ points}
  end
  
  @doc """
  Adds properties to the polyline
  """
  def add_props(pl, prop \\ []) do
    %{pl | props: Keyword.merge(pl.props, prop)}
  end

  @doc """
  Returns the bounding box of the polyline.
  """
  def bounding_box(%Geometry.Polyline{}=pl) do
    minmax = Point.bounding_box(hd(pl.points))
    Enum.reduce(tl(pl.points), minmax, fn(p, {x1, y1, x2, y2}) ->
      {xx1, yy1, xx2, yy2} = Point.bounding_box(p)
      {xmin, xmax} = Enum.min_max([x1, x2, xx1, xx2])
      {ymin, ymax} = Enum.min_max([y1, y2, yy1, yy2])
      {xmin, ymin, xmax, ymax}
    end)
  end
  
end
