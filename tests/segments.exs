Mix.install([
  {:geometrex, path: ".."}
])

defmodule Tests.Segments do

  alias Geometry.Point
  alias Geometry.Segment
  alias Geometry.Figure
  alias Math.Prime
  alias Math.Walk
  require SVG

  # Manually defined segments
  def test_1({fname}) do

    f =   Figure.new()
    # Origin
    |> Figure.add(Point.new(0, 0, r: 5, fill: :red))
    |> Figure.add(Segment.new(0, 0, 10, 0, stroke: :green, "stroke-width": 3))
    |> Figure.add(Segment.new(10, 0, 10, 10, stroke: :blue, "stroke-width": 2))
    |> Figure.add(Segment.new(
        Point.new(10, 10),
        Point.new(0, 0),
        stroke: :orange, "stroke-width": 1))
    

    # Create an SVG with a 20x20 grid and a white background, render
    # the figure on it and write it to a file
    SVG.new(f, 20, 20, bg: :white, border: 5, grid: :back)
    |> SVG.render()
    |> SVG.write(fname)
  end

  
  # 'tableau de fils' quand ma mère était jeune...
  def test_2({max, fname}) do

    # 2 points one on the X and one on Y axis then make a segment
    f = Enum.reduce(
      1..max,
      Figure.new(),
      fn(i, fig) ->
        p1 = Point.new(i, 0, r: 3, fill: :green)
        p2 = Point.new(0, max-i+1, r: 3, fill: :blue)
        Figure.add(fig, [p1, p2, Segment.new(p1, p2, stroke: :red)])
      end
    )

    # Origin
    f = Figure.add(f, Point.new(0, 0, r: 3, fill: :red))

    # Create an SVG with a 20x20 grid and a white background, render
    # the figure on it and write it to a file
    SVG.new(f, 20, 20, bg: :white, border: 5, grid: :back)
    |> SVG.render()
    |> SVG.write(fname)
  end

  # test segments with static endpoints 
  def static_ep({fname}) do
    ep = Point.new(fill: :red, r: 3)
    f = Figure.new()
    |> Figure.add(Segment.new(0, 00, 10, 00, endpoint: ep, stroke: :blue))
    |> Figure.add(Segment.new(0, 10, 10, 10, endpoint: ep, stroke: :green))
    |> Figure.add(Segment.new(0, 20, 10, 20, endpoint: ep, stroke: :orange))
    
    SVG.new(f, 10, 10, bg: :white, border: 10)
    |> SVG.render()
    |> SVG.write(fname)
  end

  # test segments with dynamic endpoints 
  def dynamic_ep({fname}) do
    ep = fn(%Point{}=p, n) ->
      if n == 0 do
        Point.new(p.x, p.y, r: 5, fill: :red)
      else
        Point.new(p.x, p.y, r: 3, fill: :green)
      end
    end
    
    f = Figure.new()
    |> Figure.add(Segment.new(0, 00, 10, 00, endpoint: ep, stroke: :blue))
    |> Figure.add(Segment.new(0, 10, 10, 10, endpoint: ep, stroke: :green))
    |> Figure.add(Segment.new(0, 20, 10, 20, endpoint: ep, stroke: :orange))
    
    SVG.new(f, 10, 10, bg: :white, border: 10)
    |> SVG.render()
    |> SVG.write(fname)
  end

      
end


tests = [
  %{desc: "Manually crafed segments",
    func: &Tests.Segments.test_1/1,
    prms: {"segments-1.svg"}
   },
  %{desc: "'Tableau de fils' in french from the 70's. 30x30 with grid",
    func: &Tests.Segments.test_2/1,
    prms: {30, "segments-2.svg"}
   },
  %{desc: "Segments with statically defined endpoints",
    func: &Tests.Segments.static_ep/1,
    prms: {"segments-static-ep.svg"}
   },
  %{desc: "Segments with dynamically calculated endpoints",
    func: &Tests.Segments.dynamic_ep/1,
    prms: {"segments-dynamic-ep.svg"}
   }
]

IO.puts("Testing segments")
Enum.map(tests, fn(p) ->
  IO.puts("-- Test: #{p.desc} -> #{p.prms |> Tuple.to_list |> List.last}")
  {f, p} = {p.func, p.prms}
  f.(p)
end)
