Mix.install([
  {:geometrex, path: ".."}
])

defmodule Tests.Points do

  alias Geometry.Point
  alias Geometry.Figure
  require SVG

  # just three points
  def test_0({fname}) do
    f = Figure.new
    |> Figure.add(Point.new(-5, -5, r: 5, fill: :green))
    |> Figure.add(Point.new(0, 0, r: 3, fill: :red))
    |> Figure.add(Point.new(5, 5, r: 5, fill: :green))

    # Create an SVG with a 20x20 grid with white bg, render the figure on it
    # and write it to a file
    SVG.new(f, 20, 20, bg: :white, border: 10)
    |> SVG.render()
    |> SVG.write(fname)
  end

  
  # some points on the X and Y axes
  def test_1({fname}) do

    f = Enum.reduce(
      -5..5,
      Figure.new(),
      fn(x, fig) ->
        p1 = Point.new(x, 0, r: 5, fill: :green)
        p2 = Point.new(0, x, r: 5, fill: :blue)
        Figure.add(fig, [p1, p2])
      end
    )

    # Origin
    f = Figure.add(f, Point.new(0, 0, r: 3, fill: :red))

    # Create an SVG with a 20x20 grid with white bg, render the figure on it
    # and write it to a file
    SVG.new(f, 20, 20, bg: :white, border: 5)
    |> SVG.render()
    |> SVG.write(fname)
  end

  
  # Same as above but with a grid with different scales on X and Y
  def test_2({xs, ys, fname}) do

    f = Enum.reduce(
      -5..5,
      Figure.new(),
      fn(x, fig) ->
        p1 = Point.new(x, 0, r: 5, fill: :green)
        p2 = Point.new(0, x, r: 5, fill: :blue)
        Figure.add(fig, [p1, p2])
      end
    )

    # Origin
    f = Figure.add(f, Point.new(0, 0, r: 3, fill: :red))

    # Create an SVG with a 40x10 grid with white bg, render the figure on it
    # and write it to a file
    SVG.new(f, xs, ys, bg: :white, border: 5)
    |> SVG.render()
    |> SVG.write(fname)
  end
  
end

tests = [
  %{desc: "Just 3 points: the origin and two corners",
    func: &Tests.Points.test_0/1,
    prms: {"points-test-0.svg"}
   },
  %{desc: "Some points on the X and Y axis",
    func: &Tests.Points.test_1/1,
    prms: {"points-test-1.svg"}
   },
  %{desc: "Some points on the X and Y axis. Different X and Y scale",
    func: &Tests.Points.test_2/1,
    prms: {40, 20, "points-test-2.svg"}
  }
]

IO.puts("Testing points")
Enum.map(tests, fn(p) ->
  IO.puts("-- Test: #{p.desc} -> #{p.prms |> Tuple.to_list |> List.last}")
  {f, p} = {p.func, p.prms}
  f.(p)
end)

