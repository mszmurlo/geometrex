Mix.install([
  {:geometrex, path: ".."}
])

defmodule Tests.Polylines do
  
  alias Geometry.Polyline
  alias Geometry.Point
  alias Geometry.Figure
  require SVG

  def test_1({fname}) do
    
    f = Figure.new()
    |> Figure.add(Polyline.new([0, 0, 1, 0, 1, 1, -1, 1, -1, -1, 2, -1, 2, 2, -2, 2, -2, -2, 3, -2],
        stroke: :blue,
        "stroke-width": 2))
    |> Figure.add(Point.new(0, 0, r: 3, fill: :red))

    SVG.new(f, 20, 20, border: 5, bg: :"#F0F0F0", grid: :back)
    |> SVG.render()
    |> SVG.write(fname)
  end

  
  def with_endpoint({ep, fname}) do
    f = Figure.new()
    |> Figure.add(Polyline.new([0, 0, 1, 0, 1, 1, -1, 1, -1, -1, 2, -1, 2, 2, -2, 2, -2, -2, 3, -2],
        stroke: :blue,
        "stroke-width": 2,
        endpoint: ep))
        |> Figure.add(Point.new(0, 0, r: 2, fill: :red))
    
    SVG.new(f, 40, 40, border: 50, grid: :back, bg: :"#F0F0F0")
    |> SVG.render()
    |> SVG.write(fname)
    
  end
end

static_ep = Geometry.Point.new(fill: :orange, r: 5)

dynamic_ep = fn(p, n) ->
  colors = %{0 => :blue, 1 => :green, 3 => :red}
  Geometry.Point.new(p.x, p.y, r: 3*(n+1), fill: colors[rem(n, 3)])
end 

tests = [
  %{desc: "a handcrafted simple spiral",
    func: &Tests.Polylines.test_1/1,
    prms: {"polylines-test-1.svg"}
   },
  %{desc: "a handcrafted simple spiral with static endpoints",
    func: &Tests.Polylines.with_endpoint/1,
    prms: {static_ep, "polylines-static-ep.svg"}
   },
  %{desc: "a handcrafted simple spiral with dynamic endpoints",
    func: &Tests.Polylines.with_endpoint/1,
    prms: {dynamic_ep, "polylines-dynamic-ep.svg"}
   }
]

IO.puts("Testing points")
Enum.map(tests, fn(p) ->
  IO.puts("-- Test: #{p.desc} -> #{p.prms |> Tuple.to_list |> List.last}")
  {f, p} = {p.func, p.prms}
  f.(p)
end)
