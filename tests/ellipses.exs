Mix.install([
  {:geometrex, path: ".."}
])

defmodule Tests.Ellipses do

  alias Geometry.Ellipse
  alias Geometry.Circle
  alias Geometry.Point
  alias Geometry.Figure
  require SVG

  def test_1({nbp, gx, gy, fname}) do
  
    brx = 30 # big axis of the big ellipse
    bry = 20 # small axis of the big ellipse
    sr = 4 # radius of the small circle
    
    f = Figure.new()
    |> Figure.add(Ellipse.new(0, 0, brx, bry, stroke: :blue, "stroke-width": 2))
    |> Figure.add(Point.new(0, 0, r: 3, fill: :red))

    f = Enum.reduce(1..nbp, f, fn(i, fig) ->
      alpha = Math.deg2rad(360/nbp*i)
      {x, y} = {:math.cos(alpha)*brx, :math.sin(alpha)*bry}
      Figure.add(fig, Circle.new(x, y, sr, fill: :green))
    end)
    
    SVG.new(f, gx, gy, bg: :"#F0F0F0", border: 100, grid: :back)
    |> SVG.render()
    |> SVG.write(fname)
  end
end

tests = [
  %{desc: "Green circles on an ellipse",
    func: &Tests.Ellipses.test_1/1,
    prms: {8, 20, 20, "ellipses-test-1.svg"}
   },
  %{desc: "Green circles on an ellipse. Y scale smaller than X",
    func: &Tests.Ellipses.test_1/1,
    prms: {8, 20, 10, "ellipses-test-2.svg"}
   },
  %{desc: "Green circles on an ellipse. X scale smaller than Y",
    func: &Tests.Ellipses.test_1/1,
    prms: {8, 10, 20, "ellipses-test-3.svg"}
   }
]

IO.puts("Testing args")
Enum.map(tests, fn(p) ->
  IO.puts("-- Test: #{p.desc} -> #{p.prms |> Tuple.to_list |> List.last}")
  {f, p} = {p.func, p.prms}
  f.(p)
end)
