Mix.install([
  {:geometrex, path: ".."}
])

defmodule Tests.Arcs do

  alias Geometry.Arc
  alias Geometry.Segment
  alias Geometry.Ellipse
  alias Geometry.Point
  alias Geometry.Figure
  require SVG

  def test_1({fname}) do
    cx = 0
    cy = 0
    rx = 20
    ry = 10
   
    f = Figure.new()
    |> Figure.add(Ellipse.new(cy, cy, rx, ry, fill: :none, stroke: :lightgrey, "stroke-width": 1))
    |> Figure.add(Arc.new(cx, cy, rx, ry, 0, 0, 270, fill: :none, stroke: :blue))
    |> Figure.add(Arc.new(cx, cy, rx, ry, 45, 0, 270, fill: :none, stroke: :red))
    |> Figure.add(Arc.new(cx, cy, rx, ry, -45, 0, 270, fill: :none, stroke: :green))
    |> Figure.add(Arc.new(cx, cy, rx, ry, 90, 0, 270, fill: :none, stroke: :black))
    |> Figure.add(Point.new(0, 0, r: 5, fill: :red))
    
    SVG.new(f, 20, 20, bg: :white, border: 10, grid: :back, grid_step: 2)
    |> SVG.render()
    |> SVG.write(fname)
  end

  def test_2({fname}) do
    cx = 0
    cy = 0
    rx = 20
    ry = 10
   
    f = Figure.new()
    |> Figure.add(Ellipse.new(cy, cy, rx, ry, fill: :none, stroke: :lightgrey, "stroke-width": 1))
    
    f = 0..360//10
    |> Enum.take_every(3)
    |> Enum.reduce(f, fn(phi, fig) ->
      Figure.add(fig, Arc.new(cx, cy, rx, ry, 0, phi, phi+10, stroke: :red, "stroke-width": 3))
      end)
    
    f = 0..360//10
    |> Enum.take_every(2)
    |> Enum.reduce(f, fn(phi, fig) ->
      Figure.add(fig, Arc.new(cx, cy, rx, ry, 45, phi, phi+10, stroke: :blue, "stroke-width": 3))
      end)
    
    f = 0..360//10
    |> Enum.reduce(f, fn(phi, fig) ->
      Figure.add(fig, Arc.new(cx, cy, rx, ry, -45, phi, phi+5, stroke: :green, "stroke-width": 3))
      end)

    f = Figure.add(f, Point.new(0, 0, r: 5, fill: :red))
    
    SVG.new(f, 20, 20, bg: :white, border: 10, grid: :back, grid_step: 2)
    |> SVG.render()
    |> SVG.write(fname)
  end

  
  def donut({fname}) do
    f = 0..360//5
    |> Enum.reduce(Figure.new(), fn(phi, fig) ->
      Figure.add(fig, Arc.new(0, 0, 30, 10, phi, 0, 359, stroke: :black, "stroke-width": 1))
      end)
    |> Figure.add(Point.new(0, 0, r: 5, fill: :red))
    
    SVG.new(f, 20, 20, bg: :white, border: 10, grid: :back, grid_step: 2)
    |> SVG.render()
    |> SVG.write(fname)
  end

  
  def rolling_ellipse({fname}) do
    f = 0..100//20
    |> Enum.reduce(Figure.new(), fn(xy, fig) ->
      Figure.add(fig, Arc.new(xy, xy, 30, 10, xy, 0, 359, stroke: :black, "stroke-width": 3))
      end)
    |> Figure.add(Segment.new(0, 0, 100, 100, stroke: :blue, "stroke-width": 3))
    |> Figure.add(Point.new(0, 0, r: 5, fill: :red))

    # centers of the ellipses above the blue line
    f = 0..100//20
    |> Enum.reduce(f, fn(xy, fig) ->
      Figure.add(fig, Point.new(xy, xy, r: 5, fill: :orange))
      end)

    SVG.new(f, 10, 10, bg: :white, border: 10, grid: :back, grid_step: 5)
    |> SVG.render()
    |> SVG.write(fname)
  end

  

  def test_static_endpoint({fname}) do
    cx = 0
    cy = 0
    rx = 20
    ry = 10
   
    f = Figure.new()
    |> Figure.add(Ellipse.new(cy, cy, rx, ry, fill: :none, stroke: :lightgrey, "stroke-width": 1))
    
    f = 0..360//10
    |> Enum.take_every(3)
    |> Enum.reduce(f, fn(phi, fig) ->
      Figure.add(fig, Arc.new(cx, cy, rx, ry, 0, phi, phi+10,
            stroke: :red, "stroke-width": 3,
            endpoint: Point.new(0, 0, fill: :green, r: 5)))
      end)
    |> Figure.add(Point.new(0, 0, r: 5, fill: :red))
    
    SVG.new(f, 20, 20, bg: :white, border: 10, grid: :back, grid_step: 2)
    |> SVG.render()
    |> SVG.write(fname)
  end

  
  def test_dynamic_endpoint({fname}) do
    rx = 20
    ry = 10

    # the endpoint calculation finction
    # p is the template Point
    # n is 0 for the first point and 1 for the second point
    ep = fn(%Point{}=p, n) ->
      if n == 0 do
        Point.new(p.x, p.y, r: 5, fill: :red)
      else
        Point.new(p.x, p.y, r: 3, fill: :green)
      end
    end
   
    f = Figure.new()
    |> Figure.add(Ellipse.new(10, 10, rx, ry, fill: :none, stroke: :lightgrey, "stroke-width": 1))
    
    f = 0..360//10
    |> Enum.take_every(2)
    |> Enum.reduce(f, fn(phi, fig) ->
      Figure.add(fig, Arc.new(10, 10, rx, ry, 20, phi, phi+10,
            stroke: :red, "stroke-width": 3,
            endpoint: ep))
      end)
    |> Figure.add(Point.new(0, 0, r: 5, fill: :red))
    |> Figure.add(Point.new(10, 10, r: 3, fill: :green))
    
    SVG.new(f, 20, 20, bg: :white, border: 10, grid: :back, grid_step: 1)
    |> SVG.render()
    |> SVG.write(fname)
  end
end


tests = [
  %{desc: "Elliptic arcs with different colors",
    func: &Tests.Arcs.test_1/1,
    prms: {"arcs-test-1.svg"}
   },
  %{desc: "Elliptic arcs with different colors dashed strokes",
    func: &Tests.Arcs.test_2/1,
    prms: {"arcs-test-2.svg"}
   },
  %{desc: "Elliptic donut",
    func: &Tests.Arcs.donut/1,
    prms: {"arcs-donut.svg"}
   },
  %{desc: "Rollong ellipse",
    func: &Tests.Arcs.rolling_ellipse/1,
    prms: {"arcs-rolling-ellipse.svg"}
   },
  %{desc: "Elliptic arcs with statically defined endpoints",
    func: &Tests.Arcs.test_static_endpoint/1,
    prms: {"arcs-with-static-endpoints.svg"}
   },
  %{desc: "Elliptic arcs with dynamically defined endpoints",
    func: &Tests.Arcs.test_dynamic_endpoint/1,
    prms: {"arcs-with-dynamic-endpoints.svg"}
   },
]

IO.puts("Testing args")
Enum.map(tests, fn(p) ->
  IO.puts("-- Test: #{p.desc} -> #{p.prms |> Tuple.to_list |> List.last}")
  {f, p} = {p.func, p.prms}
  f.(p)
end)
