Mix.install([
  {:geometrex, path: ".."}
])

defmodule Tests.Circles do

  alias Geometry.Circle
  alias Geometry.Point
  alias Geometry.Figure
  require SVG

  
  def test_1({nbp, gx, gy, fname}) do

    br = 40 # radius of the big circle
    sr = 4  # radius of the small circle
    f = Figure.new()
    |> Figure.add(Circle.new(0, 0, br, stroke: :blue, "stroke-width": 2))
    |> Figure.add(Point.new(0, 0, r: 5, fill: :red))

    f = Enum.reduce(1..nbp, f, fn(i, fig) ->
      alpha = Math.deg2rad(360/nbp*i)
      {x, y} = {:math.cos(alpha)*br, :math.sin(alpha)*br}
      Figure.add(fig, Circle.new(x, y, sr, fill: :green))
    end)

    # Create an SVG with a 20x20 grid with white bg, render the figure on it
    # and write it to a file
    SVG.new(f, gx, gy, border: 100, bg: :"rgb(250, 250, 250)", grid: :back)
    |> SVG.render()
    |> SVG.write(fname)
  end

  
end

tests = [
  %{desc: "Green circles on a cirle",
    func: &Tests.Circles.test_1/1,
    prms: {12, 20, 20, "circles-test-1.svg"}
   },
  %{desc: "Green circles on a cirle, smaller Y scale",
    func: &Tests.Circles.test_1/1,
    prms: {12, 20, 10, "circles-test-2.svg"}
   },
  %{desc: "Green circles on a cirle, smaller X scale",
    func: &Tests.Circles.test_1/1,
    prms: {12, 10, 20, "circles-test-3.svg"}
   }
]

IO.puts("Testing args")
Enum.map(tests, fn(p) ->
  IO.puts("-- Test: #{p.desc} -> #{p.prms |> Tuple.to_list |> List.last}")
  {f, p} = {p.func, p.prms}
  f.(p)
end)
