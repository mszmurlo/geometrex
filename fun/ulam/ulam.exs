Mix.install([
  {:geometrex, path: "../.."}
])

defmodule Ulam do

  alias Geometry.Point
  alias Geometry.Segment
  alias Geometry.Figure
  alias Math.Walk
  alias Math.Prime
  require SVG

  @doc """
  Draws the Ulam's spiral.

  Parameters are tuples so that they can be send by a 
  generic function call. 

  Parameters:
  - sz: the size of the side of the square where the spiral is drawn
  - fname: the name of the file where to write the svg
  """
  def ulam_1({sz, fname}) do
    f = Enum.reduce(2..sz*sz,
      Figure.new(),
      fn(n, fig) ->
        {x, y} = Walk.ulam_n2xy(n)
        Figure.add(fig, if Prime.is_prime(n) do
              Point.new(x,y, r: 5, fill: :red)
            else
              Point.new(x,y, r: 2, fill: :gray)
            end)
      end)

    # Origin
    f = Figure.add(f, Point.new(0, 0, r: 5, fill: :black))

    # Create an SVG with a 10 grid with white bg, render the figure on it
    # and write it to a file
    SVG.new(f, 10, 10, bg: :white, border: 10)
    |> SVG.render()
    |> SVG.write(fname)
    
  end

  

  defp mk_segs(prev, [c]) do
    Segment.new(prev, c, stroke: :black, "stroke-width": 2)
  end
  defp mk_segs(prev, [c | t]) do
    [
      Segment.new(prev, c, stroke: :black, "stroke-width": 2),
      mk_segs(c, t)
    ]
  end
  
  @doc """
  Draws the Ulam's spiral with lines between points.
  """
  def ulam_segments({sz, fname}) do
    points = Enum.map(2..sz*sz, fn(n) ->
      {x, y} = Walk.ulam_n2xy(n)
      if Prime.is_prime(n) do
        Point.new(x, y, r: 5, fill: :red)
      else
        Point.new(x, y, r: 2, fill: :gray)
      end
    end)

    segs = mk_segs(Point.new(0, 0), points)

    f = Figure.new()
    |> Figure.add(segs)
    |> Figure.add(points)
    |> Figure.add(Point.new(0, 0, r: 5, fill: :black))

    # Create an SVG with a 10 grid with white bg, render the figure on it
    # and write it to a file
    SVG.new(f, 20, 20, bg: :white, border: 10)
    |> SVG.render()
    |> SVG.write(fname)
  end

  
  @doc """
  Draws the Ulam's spiral styles with CSS. Each point (number) on 
  the spiral is tagged with a property `"prime"` which can take values
  `"f"` or `"t"`. This promerty is than matched in the CSS to allocate
  a style

  Parameters
  - sz: the size on on side of the square where the spiral is drawn
  - fname: the name of the file where to write the svg
  """
  def ulam_css({sz, css, prime_func, fname}) do
    f = Enum.reduce(2..sz*sz,
      Figure.new(),
      fn(n, fig) ->
        {x, y} = Walk.ulam_n2xy(n)
        Figure.add(fig, prime_func.(x, y, n))
      end)

    # Origin
    f = Figure.add(f, Point.new(0, 0, class: :origin))

    # Create an SVG with a 10 grid with white bg, render the figure on it
    # and write it to a file
    SVG.new(f, 10, 10, border: 10)
    |> SVG.add_style(css)
    |> SVG.render()
    |> SVG.write(fname)
    
  end
  
end

simple_css = """
svg { background-color: ghostwhite; }
.origin { fill: red; r: 4; }
circle { fill: lightgrey; r: 2; }
[prime="t"] { stroke: red; stroke-width: 1; r: 5; fill: orange; }
"""

# Returns a point at coordinates (x, y) with property .fx that can
# take values from 1 to 10
simple_prime = fn(x, y, n) ->
  if Math.Prime.is_prime(n) do
    Geometry.Point.new(x,y, prime: :t)
  else
    Geometry.Point.new(x,y, prime: :f)
  end
end

complex_css = """
svg { background-color: ghostwhite; }
.origin { fill: red; r: 4; }
.f0  { r: 1; fill: rgb(0, 0, 0)}
.f1  { r: 2; fill: rgb(26, 26, 26)}
.f2  { r: 3; fill: rgb(52, 52, 52)}
.f3  { r: 4; fill: rgb(78, 78, 78)}
.f4  { r: 5; fill: rgb(104, 104, 104)}
.f5  { r: 6; fill: rgb(130, 130, 130)
.f6  { r: 7; fill: rgb(156, 156, 156)}
.f7  { r: 8; fill: rgb(182, 182, 182)}
.f8  { r: 9; fill: rgb(208, 208, 208)}
.f9  { r: 10; fill: rgb(234, 234, 234)}
"""

# Returns a point at coordinates (x, y) with class set 'f0' for primes
# and for non primes, class set to the value of the rds() of n
rds_non_prime = fn(x, y, n) ->
  if Math.Prime.is_prime(n) do
    Geometry.Point.new(x, y, class: :f0)
  else
    Geometry.Point.new(x, y, class: :"f#{Math.rds(n)}")
  end
end

# The opposite of the previous one: non-primes are set to class 'f0'
# and rds() is calculated for primes
rds_on_prime = fn(x, y, n) ->
  if Math.Prime.is_prime(n) do
    Geometry.Point.new(x, y, class: :"f#{Math.rds(n)}")
  else
    Geometry.Point.new(x, y, class: :f0)
  end
end

# primes as 'f0' then 'f1', 'f2', etc will be assigned to non-primes
# with 3, 4, etc factors
factors_on_non_prime = fn(x, y, n) ->
  if Math.Prime.is_prime(n) do
    Geometry.Point.new(x, y, class: :f0)
  else
    nbf = Math.Prime.factors(n) |> Enum.count
    nbf = if nbf>10, do: 9, else: nbf 
    Geometry.Point.new(x, y, class: :"f#{nbf-2}")
  end
end



tests = [
  %{desc: "Ulam spiral with points",
    func: &Ulam.ulam_1/1,
    prms: {100, "ulam.svg"}
   },
  %{desc: "Ulam spiral with points styled with css sheet",
    func: &Ulam.ulam_css/1,
    prms: {100, simple_css, simple_prime, "ulam-simple-css.svg"}
  },
  %{desc: "Ulam spiral with points styled with css (rds calculated on non-primes)",
    func: &Ulam.ulam_css/1,
    prms: {100, complex_css, rds_non_prime, "ulam-rds-on-non-prime-css.svg"}
  },
  %{desc: "Ulam spiral with points styled with css (rds calculated on primes)",
    func: &Ulam.ulam_css/1,
    prms: {100, complex_css, rds_on_prime, "ulam-rds-on-prime-css.svg"}
  },
  %{desc: "Ulam spiral with points styled with css (nb of factors calculated on non-primes)",
    func: &Ulam.ulam_css/1,
    prms: {100, complex_css, factors_on_non_prime, "ulam-factors-on-non-prime-css.svg"}
  },
  %{desc: "Ulam spiral with segments that show the spiral",
    func: &Ulam.ulam_segments/1,
    prms: {40, "ulam-segments.svg"}
  }
]

IO.puts("\nDrawing Ulam spirals")
Enum.map(tests, fn(p) ->
  IO.puts("-- Test: #{p.desc} -> #{p.prms |> Tuple.to_list |> List.last}")
  {f, p} = {p.func, p.prms}
  f.(p)
end)

