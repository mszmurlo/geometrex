# Geometrex

Geometrex is an Elixir library that generates SVG graphics. It's
currently in a very, very early development stage and many things are
missing and existing ones may change in the near future so don't
expect it to be stable. Geometrex is my current weekend hobby so don't
either expect fast implementation of new features. 

Yet, if you want to give a hand or correct some bugs, or write some
documentation, I'd be happy to merge your propositions.


## Goal

* Elixir is fun, graphics is entertaining and basic SGV
  is easy so merge both
* No, seriously, implement a charting lib and/or a geographical map
  renderer in some future.
* Even more seriously, have even more fun!


## Usage

* Add Geometrex dependency to your `mix.esx` file:
  
  ```
  defp deps do
    [
      {:geometrex, git: "https://gitlab.com/mszmurlo/geometrex.git"}
    ]
  ```

* Use it in your code (we assume here it's a CLI application):
  
  ```elixir
  defmodule SvgTest do
  
    alias Geometry.Point
    alias Geometry.Figure
    require SVG
  
    def main(args) do
      # Create a figure
      f = Figure.new
      |> Figure.add(Point.new(-5, -5, r: 5, fill: :green))
      |> Figure.add(Point.new(0, 0, r: 3, fill: :red))
      |> Figure.add(Point.new(5, 5, r: 5, fill: :green))
  
      # generate the SVG
      SVG.new(f, 20, 20, bg: :white, border: 10, grid: :back)
      |> SVG.render()
      |> SVG.write("points.svg")
  
      IO.puts("Generated points.svg'")
    end
  ```
  
  `points.svg` renders as three points, the origin in red, and two
  green points drawn on a grid:
  
  ![](points.png)

* Check the `tests` directory to some examples.

## How it works

There is no formal documentation for the moment. See
- `tests/*exs` for simple examples
- `fun/*/*.exs` for some more fun examples


## Todo

### Mains

* Implement rectangles
* Implement polygons
* Implement diamonds
* Implement text drawing
* Implement maps processing and rendering (this one will be fun!)
* Add a pseudo-property to instruct the engine to non-adding the
  default properties. Might be useful whan CSS styling is used

### Other

* Add a pseudo-prop to draw polylines / polygons angles "smoothly"
  probably with splines

* Separate the core from the usages like the walks, prime calculation,
  maps when implemented, etc

* Implement transforms like translation, rotation, skew, etc

* For grids on SVG, add the color and thickness props. Make something
  like `grid: %{pos: : front, step: 2, color: :some_color}`

* Remove the pseudo-properties `is-point` and `is-circle` and add to 
  all elements a `type:` property with the valid value

* Add the possibility to remove a property from an object

* Add the possibility to use any other element than a `Point` as an endpoint

* Improve / rewrite source code documentation
* Rewrite correct and clean documentation

* Implement topological functions like `is_inside(point, polygon)`,
  `intersection(line, line)`, `union(...)`, `difference(...)`,
  `convex_hull(...)`
  
### Really needed ?
* Implement paths and/or turtle 
* Add multiple figures on the same SVG
* Implement all line-like figures as path or polylines

### Done (just to track previous *Todos*



## References
  
* SVG
  * https://svgwg.org/svg2-draft/paths.html#PathElement
  * https://developer.mozilla.org/fr/docs/Web/SVG
  
* Inspiration
  * https://github.com/DannyBen/victor
  * https://github.com/summasmiff/svg_generator

* How to draw an ellipse with path: 
  * http://complexdan.com/svg-circleellipse-to-path-converter/
  * https://observablehq.com/@toja/ellipse-and-elliptical-arc-conversion

* Art
  * https://fineartamerica.com/art/digital+art/algorithmic
  * https://fineartamerica.com/art/digital+art
  * https://offscreencanvas.com/issues/
  * https://observablehq.com/@osteele/truchet-tile-generation

* Other
  * *Merveilleux nombres premiers*, Jean-Paul Delahaye, Éditions Belin - 
    Pour la science, 2000
    
  
